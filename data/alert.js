import { mdiAlertCircleOutline } from '@mdi/js'

export default {
  type: 'info',
  icon: mdiAlertCircleOutline,
  active: false,
  text: 'This site is still a work in progress 🙈',
}
