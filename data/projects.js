import tech from './technologies'

export default [
  {
    id: 'jfawölfkwaf',
    name: 'Blog Template',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et',
    image: 'blog-template.png',
    demo: 'https://serene-varahamihira-5a3712.netlify.app/',
    code: 'https://gitlab.com/julian-storyblok-projects/base_template_v2',
    highlight: true,
    techStack: [
      tech.vue,
      tech.nuxt,
      tech.vuetify,
      tech.storyblok,
      tech.netlify,
    ],
    url: 'https://serene-varahamihira-5a3712.netlify.app/',
  },
  {
    id: 'jfwjaflkwjf',
    name: 'Old Portfolio',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,',
    image: 'old-portfolio.png',
    demo: 'https://62ef0176ac35f82daed9397a--incomparable-naiad-20d9f2.netlify.app/',
    highlight: true,
    techStack: [
      tech.vue,
      tech.nuxt,
      tech.vuetify,
      tech.storyblok,
      tech.netlify,
    ],
  },
  {
    id: 'fwkamdlad',
    name: 'Portfolio',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore mconsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore m consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,',
    image: 'portfolio.png',
    code: 'https://gitlab.com/julian-storyblok-projects/portfolio',
    highlight: true,
    techStack: [
      tech.vue,
      tech.nuxt,
      tech.vuetify,
      tech.storyblok,
      tech.netlify,
    ],
  },
  {
    id: 'jfeklajdlkw',
    name: 'Name4',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore mconsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore m consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,',
    image: 'blog-template.png',
    demo: 'https://serene-varahamihira-5a3712.netlify.app/',
    highlight: false,
    techStack: [tech.vue],
  },
  {
    id: 'gejifjioajd',
    name: 'Name5',
    description:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore mconsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore m consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,',
    image: 'blog-template.png',
    demo: 'https://serene-varahamihira-5a3712.netlify.app/',
    highlight: false,
    techStack: [tech.angular],
  },
]
