import { mdiGithub, mdiGitlab, mdiLinkedin } from '@mdi/js'

export default [
  {
    url: 'https://www.linkedin.com/in/julian-wendling/',
    title: 'LinkedIn Profile',
    icon: mdiLinkedin,
  },
  {
    url: 'https://gitlab.com/deaven',
    title: 'Gitlab Profile',
    icon: mdiGitlab,
  },
  {
    url: 'https://github.com/AkaiRingo',
    title: 'Github Profile',
    icon: mdiGithub,
  },
]
