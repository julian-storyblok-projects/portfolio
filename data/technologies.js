export default {
  vue: {
    id: 'fhwadwajd',
    name: 'Vue',
    url: 'https://vuejs.org/',
    image: 'vue-logo.png',
    color: '#41b883'
  },
  vuetify: {
    id: 'hwjadklwad',
    name: 'Vuetify',
    url: 'https://vuetifyjs.com/',
    image: 'vuetify-logo.png',
    color: '#7bc6ff'
  },
  nuxt: {
    id: 'djawdhjwhf',
    name: 'Nuxt',
    url: 'https://nuxtjs.org/',
    image: 'nuxt-logo.png',
    color: '#00dc82'
  },
  storyblok: {
    id: 'dwhadhawd',
    name: 'Storyblok',
    url: 'https://www.storyblok.com/',
    image: 'storyblok-logo.png',
    color: '#0ab3af'
  },
  netlify: {
    id: 'dhwjakdhwad',
    name: 'Netlify',
    url: 'https://app.netlify.com/',
    image: 'netlify-logo.png',
    color: '#25c7b7'
  },
  angular: {
    id: 'hdkwjhadwad',
    name: 'Angular',
    url: 'https://angular.io/',
    image: 'angular-logo.png',
    color: '#dd0031'
  },
  unity: {
    id: 'dwhadhwfafff',
    name: 'Unity',
    url: 'https://unity.com/',
    image: 'unity-logo.png',
    color: '#222c37'
  },
}
